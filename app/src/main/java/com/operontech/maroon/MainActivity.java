/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.operontech.maroon.frag.*;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	public final static String DAWGTAG = "DawgTrails";
	private boolean doubleBackToExitPressedOnce = false;
	FragmentManager fManager;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Create the FragmentManager for the id/frag holder
		fManager = getSupportFragmentManager();
		fManager.beginTransaction().add(R.id.frag, new FragmentHome()).commit();

		setContentView(R.layout.activity_main);

		// Setup the toolbar
		final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		// Setup the navigation drawer
		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		// Setup the listener for the navigationView
		final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		// Restore saved data, if necessary.
		if (savedInstanceState != null) {
			setTitle(savedInstanceState.getCharSequence("title"));
			final Fragment frag = fManager.getFragment(savedInstanceState, "currentFragment");
			showFragment(frag);
		}

		verifyNeededPermissions();

		// Prepare MapBox Account Manager
		MapboxAccountManager.start(getApplicationContext(), getString(R.string.mapbox_access_token));

		fManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {
				if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
					getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
					toolbar.setNavigationOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(final View v) {
							onBackPressed();
						}
					});
				} else {
					//show hamburger
					getSupportActionBar().setDisplayHomeAsUpEnabled(false);
					toggle.syncState();
					toolbar.setNavigationOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(final View v) {
							drawer.openDrawer(GravityCompat.START);
						}
					});
				}
			}
		});
	}

	@Override
	public void onBackPressed() {
		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else if (fManager.getBackStackEntryCount() == 0 && !doubleBackToExitPressedOnce) {
			doubleBackToExitPressedOnce = true;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);
			Toast.makeText(this, getString(R.string.back_button_exit), Toast.LENGTH_SHORT).show();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putCharSequence("title", getTitle());
		fManager.putFragment(outState, "currentFragment", fManager.findFragmentById(R.id.frag));
	}

	@Override
	public boolean onNavigationItemSelected(final MenuItem item) {

		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);

		new Handler().postDelayed(new Runnable() {
			public void run() {
				final int id = item.getItemId();

				if (id == R.id.nav_home) {
					showFragment(new FragmentHome());
				} else if (id == R.id.nav_map) {
					showFragment(new FragmentMap());

				} else if (id == R.id.nav_courses) {
					showFragment(new FragmentCourses());
				} else if (id == R.id.nav_schedule) {
					showFragment(new FragmentSchedule());

				} else if (id == R.id.nav_residence_halls) {
					showFragmentPlaces(R.string.type_residence_hall, R.string.type_residence_hall_plural);
				} else if (id == R.id.nav_academic) {
					showFragmentPlaces(R.string.type_academic_hall, R.string.type_academic_hall_plural);
				} else if (id == R.id.nav_food_and_dining) {
					showFragmentPlaces(R.string.type_food_and_dining, R.string.type_food_and_dining_plural);
				} else if (id == R.id.nav_athletic) {
					showFragmentPlaces(R.string.type_athletic, R.string.type_athletic_plural);
				} else if (id == R.id.nav_utility) {
					showFragmentPlaces(R.string.type_utility, R.string.type_utility_plural);

				} else if (id == R.id.nav_msu_website) {
					final Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.msstate.edu"));
					startActivity(browserIntent);

				} else if (id == R.id.nav_feedback) {
					showFragment(new FragmentFeedback());
				} else if (id == R.id.nav_about) {
					showFragment(new FragmentAbout());
				} else if (id == R.id.nav_prefs) {
					showFragment(new FragmentPrefs());
				}

			}
		}, 250);
		return true;
	}

	/**
	 * Displays a fragment on the "id/frag" layout
	 * @param fragment the fragment to replace the current fragment with
	 */
	public void showFragment(final Fragment fragment) {
		fManager.beginTransaction()
		        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
		        .replace(R.id.frag, fragment)
		        .commit();
	}

	/**
	 * Creates the necessary arguments for the FragmentPlaces fragment
	 * @param target the string ID for the target place type
	 * @param targetPlural the string ID for the target place type--plural
	 */
	public void showFragmentPlaces(final int target, final int targetPlural) {
		final Fragment newFragment = new FragmentPlaces();
		final Bundle nFArguments = new Bundle();
		nFArguments.putString("target", getString(target));
		nFArguments.putString("targetPlural", getString(targetPlural));
		newFragment.setArguments(nFArguments);
		showFragment(newFragment);
	}

	/**
	 * Requests the necessary permissions from the user
	 */
	public void verifyNeededPermissions() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(final Void... params) {
				if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat
						.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(MainActivity.this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION }, 0);
				}
				return null;
			}
		}.execute();
	}
}

