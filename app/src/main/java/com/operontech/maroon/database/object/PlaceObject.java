/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.database.object;

import com.mapbox.mapboxsdk.geometry.LatLng;

import java.io.Serializable;

public class PlaceObject implements Serializable {
	String name = "";
	String description = "";
	String context = "";
	double latitude = 0;
	double longitude = 0;

	public PlaceObject(final String name, final String context, final LatLng location) {
		this.name = name;
		this.context = context;
		latitude = location.getLatitude();
		longitude = location.getLongitude();
	}

	public PlaceObject() {
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getContext() {
		return context;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public LatLng getLatLng() {
		return new LatLng(latitude, longitude);
	}

	public String getLatLngString() {
		return latitude + ", " + longitude;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setDescription(final String description) {
		this.description = (description == null) ? "" : description;
	}

	public void setContext(final String context) {
		this.context = context;
	}

	public void setLatitude(final double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(final double longitude) {
		this.longitude = longitude;
	}

	public void setLatLng(final LatLng location) {
		latitude = location.getLatitude();
		longitude = location.getLongitude();

	}
}
