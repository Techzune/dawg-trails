/*
 * Copyright (c) 2017 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.database.object;

import java.util.Locale;

public enum Day {
	// @formatter:off
	SUNDAY("SU", 0),
	MONDAY("M", 1),
	TUESDAY("T", 2),
	WEDNESDAY("W", 3),
	THURSDAY("TH", 4),
	FRIDAY("F", 5),
	SATURDAY("SA", 6);
	// @formatter:on

	String abbreviation;
	int index;

	Day(final String abbreviation, final int index) {
		this.abbreviation = abbreviation;
		this.index = index;
	}

	/**
	 * Get the index of this day
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Get the abbreviation format of the day
	 * This is typically 1-2 characters long
	 * @return the abbreviation
	 */
	public String getAbrv() {
		return abbreviation;
	}

	/**
	 * Returns the name of the day with title case
	 * @return the title case formatting
	 */
	public String getTitleCase() {
		final String str = toString();
		return str.charAt(0) + str.substring(1).toLowerCase();
	}

	/**
	 * Creates a Day enum from just the name
	 * @param name the name of the day as a string
	 * @return the resulting day
	 */
	public static Day fromName(final String name) {
		return Day.valueOf(name.toUpperCase(Locale.US));
	}
}
