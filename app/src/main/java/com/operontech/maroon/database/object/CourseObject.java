/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.database.object;

import com.operontech.maroon.util.ScheduleEntry;

import java.io.Serializable;

public class CourseObject implements Serializable, Comparable<CourseObject> {
	String id = "";
	String courseSubject = "";
	String courseNumber = "";
	String sectionNumber = "";
	String creditHours = "";
	String title = "";
	String building = "";
	String roomNumber = "";
	String instructorName = "";
	ScheduleEntry scheduleEntry = new ScheduleEntry();

	/**
	 * Gets the ID for the courseObject
	 * This should be determined by the database.
	 * @return the ID
	 */
	public String getID() {
		return id;
	}

	/**
	 * Sets the ID for the courseObject
	 * This should be determined by the database.
	 * @param id the ID
	 */
	public void setID(final String id) {
		this.id = id;
	}

	/**
	 * Gets the course's subject, typically abbreviated
	 * @return the subject for the course
	 */
	public String getCourseSubject() {
		return courseSubject;
	}

	/**
	 * Sets the course's subject, typically abbreviated
	 * @param courseSubject the course's subject
	 */
	public void setCourseSubject(final String courseSubject) {
		if (courseSubject.equals("Pick One")) {
			return;
		}
		this.courseSubject = courseSubject;
	}

	/**
	 * Gets the course's number, determined by the university
	 * @return the course's number
	 */
	public String getCourseNumber() {
		return courseNumber;
	}

	/**
	 * Sets the course's number, determined by the university
	 * @param courseNumber the course's number
	 */
	public void setCourseNumber(final String courseNumber) {
		this.courseNumber = courseNumber;
	}

	/**
	 * Gets the course's section number, determined by the university
	 * @return the course's section number
	 */
	public String getSectionNumber() {
		return sectionNumber;
	}

	/**
	 * Sets the course's section number, determined by the university
	 * @param sectionNumber the course's section number
	 */
	public void setSectionNumber(final String sectionNumber) {
		this.sectionNumber = sectionNumber;
	}

	/**
	 * Gets the course's credit hours, determined by the university
	 * @return the course's credit hours
	 */
	public String getCreditHours() {
		return creditHours;
	}

	/**
	 * Sets the course's credit hours, determined by the university
	 * @param courseHours the course's credit hours
	 */
	public void setCreditHours(final String courseHours) {
		this.creditHours = courseHours;
	}

	/**
	 * Gets the course's title, can be partially abbreviated
	 * @return the course's title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the course's title, can be partially abbreviated
	 * @param title the course's title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * Gets the course's building, typically abbreviated
	 * @return the course's building
	 */
	public String getBuilding() {
		return building;
	}

	/**
	 * Sets the course's building, typically abbreviated
	 * @param building the course's building
	 */
	public void setBuilding(final String building) {
		this.building = building;
	}

	/**
	 * Gets the course's room number, determined by the university
	 * @return the course's room number
	 */
	public String getRoomNumber() {
		return roomNumber;
	}

	/**
	 * Sets the course's room number, determined by the university
	 * @param room the course's room number
	 */
	public void setRoomNumber(final String room) {
		this.roomNumber = room;
	}

	/**
	 * Gets the course's instructor's name, determined by the university
	 * @return the course's instructor's name
	 */
	public String getInstructorName() {
		return instructorName;
	}

	/**
	 * Sets the course's instructor's name, determined by the university
	 * @param instructorName the course's instructor's name
	 */
	public void setInstructorName(final String instructorName) {
		this.instructorName = instructorName;
	}

	/**
	 * Gets the course's scheduleEntry (timeStart, timeEnd, etc.)
	 * @return the course's scheduleEntry
	 */
	public ScheduleEntry getSchedule() {
		return scheduleEntry;
	}

	/**
	 * Sets the course's scheduleEntry (timeStart, timeEnd, etc.)
	 * @param scheduleEntry the course's scheduleEntry
	 */
	public void setScheduleEntry(final ScheduleEntry scheduleEntry) {
		this.scheduleEntry = scheduleEntry;
	}

	/**
	 * Gets the course's startTime from the scheduleEntry
	 * @return the course's startTime
	 */
	public TimeObject getStartTime() {
		return getSchedule().getStartTime();
	}

	/**
	 * Sets the course's startTime from the scheduleEntry
	 * @param timeObj the course's startTime
	 */
	public void setStartTime(final TimeObject timeObj) {
		getSchedule().setStartTime(timeObj);
	}

	/**
	 * Gets the course's endTime from the scheduleEntry
	 * @return the course's endTime
	 */
	public TimeObject getEndTime() {
		return getSchedule().getEndTime();
	}

	/**
	 * Sets the course's endTime in the scheduleEntry
	 * @param timeObj the course's endtime
	 */
	public void setEndTime(final TimeObject timeObj) {
		getSchedule().setEndTime(timeObj);
	}

	/**
	 * Sets all the days in the course's scheduleEntry to FALSE
	 */
	public void clearDays() {
		scheduleEntry.clearDays();
	}

	/**
	 * Changes the value of a Day in the course's scheduleEntry
	 * @param day the Day to change
	 * @param value the new value
	 */
	public void setDay(final Day day, final boolean value) {
		scheduleEntry.setDay(day, value);
	}

	@Override
	public int compareTo(final CourseObject cObj) {
		if (getStartTime() != null && cObj.getStartTime() != null) {
			return getStartTime().compareTo(cObj.getStartTime());
		}
		return 0;
	}
}
