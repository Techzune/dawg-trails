/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import com.operontech.maroon.database.object.PlaceObject;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PlacesDatabase extends SQLiteAssetHelper {

	private static final String DATABASE_NAME = "places.db";
	private static final int DATABASE_VERSION = 5;

	public interface TABLES {
		String RESIDENCE_HALLS = "ResidenceHalls";
		String ACADEMIC = "Academic";
		String FOODANDDINING = "FoodandDining";
		String ATHLETIC = "Athletic";
		String UTILITY = "Utility";
	}

	public interface COLUMNS {
		String NAME = "name";
		String TYPE = "type";
		String LATITUDE = "latitude";
		String LONGITUDE = "longitude";
		String DESCRIPTION = "description";
	}

	public PlacesDatabase(final Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		setForcedUpgrade();
	}

	/**
	 * Generates a list of PlaceObjects from the specified table
	 * @param table the table to generate a list from
	 * @return the generated list, sorted alphabetically
	 */
	public ArrayList<PlaceObject> getPlacesAsListFromTable(final String table) {
		final ArrayList<PlaceObject> returnList = new ArrayList<>();
		final SQLiteDatabase db = getReadableDatabase();
		final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(table);
		final Cursor cursor = qb.query(db, null, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			PlaceObject pObj;
			while (!cursor.isAfterLast()) {
				pObj = new PlaceObject();
				pObj.setName(getCursorString(cursor, COLUMNS.NAME));
				pObj.setDescription(getCursorString(cursor, COLUMNS.DESCRIPTION));
				pObj.setContext(getCursorString(cursor, COLUMNS.TYPE));
				pObj.setLatitude(cursor.getDouble(cursor.getColumnIndex(COLUMNS.LATITUDE)));
				pObj.setLongitude(cursor.getDouble(cursor.getColumnIndex(COLUMNS.LONGITUDE)));
				returnList.add(pObj);
				cursor.moveToNext();
			}
		}
		if (cursor != null) {
			cursor.close();
		}
		Collections.sort(returnList, new Comparator<PlaceObject>() {
			@Override
			public int compare(final PlaceObject p1, final PlaceObject p2) {
				return p1.getName().compareToIgnoreCase(p2.getName());
			}
		});
		return returnList;
	}

	private String getCursorString(final Cursor c, final String columnName) {
		return c.getString(c.getColumnIndex(columnName));
	}
}
