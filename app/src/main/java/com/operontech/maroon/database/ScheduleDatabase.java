/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.util.Log;
import com.operontech.maroon.database.object.CourseObject;
import com.operontech.maroon.database.object.Day;
import com.operontech.maroon.database.object.TimeObject;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.operontech.maroon.MainActivity.DAWGTAG;

public class ScheduleDatabase extends SQLiteOpenHelper {

	private static ScheduleDatabase databaseInstance;

	private static final String DATABASE_NAME = "schedule.db";
	private static final int DATABASE_VERSION = 1;

	private ScheduleDatabase(final Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static synchronized ScheduleDatabase getInstance(final Context context) {
		if (databaseInstance == null) {
			databaseInstance = new ScheduleDatabase(context.getApplicationContext());
		}
		return databaseInstance;
	}

	public interface TABLES {
		String SCHEDULE = "main_schedule";
	}

	public interface COLUMNS {
		String ID = "id";

		String COURSE_NUMBER = "course_number";
		String COURSE_SUBJECT = "course_subject";
		String COURSE_SECTION = "course_section";
		String COURSE_CREDIT_HOURS = "course_credit_hours";
		String COURSE_NAME = "course_name";

		String COURSE_BUILDING = "course_building";
		String COURSE_ROOM = "course_room";
		String COURSE_INSTRUCTOR = "course_instructor";

		String COURSE_TIME_START = "course_time_start";
		String COURSE_TIME_END = "course_time_end";
		String COURSE_DAYS = "course_days";
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		// @formatter:off
		db.execSQL(
				"CREATE TABLE " + TABLES.SCHEDULE + " ( " +
				COLUMNS.ID + " INTEGER NOT NULL PRIMARY KEY, " +
				COLUMNS.COURSE_SUBJECT + " TEXT DEFAULT '', " +
				COLUMNS.COURSE_NUMBER + " TEXT DEFAULT '0', " +
				COLUMNS.COURSE_SECTION + " TEXT DEFAULT '0', " +
				COLUMNS.COURSE_NAME + " TEXT DEFAULT '', " +
				COLUMNS.COURSE_CREDIT_HOURS + " TEXT DEFAULT '0', " +

				COLUMNS.COURSE_BUILDING + " TEXT DEFAULT '', " +
				COLUMNS.COURSE_ROOM + " TEXT DEFAULT '', " +
				COLUMNS.COURSE_INSTRUCTOR + " TEXT DEFAULT '', " +

				COLUMNS.COURSE_TIME_START + " TEXT DEFAULT '0:0', " +
				COLUMNS.COURSE_TIME_END + " TEXT DEFAULT '0:0', " +
				COLUMNS.COURSE_DAYS + " TEXT DEFAULT '' )");
		// @formatter:on
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
	}

	public long addCourse(final CourseObject courseObj) {
		return getReadableDatabase().insert(TABLES.SCHEDULE, null, compileContentValues(courseObj));
	}

	public int updateCourse(final CourseObject courseObj) {
		if (courseObj.getID().equals("")) {
			return (int) addCourse(courseObj);
		}
		return getReadableDatabase().update(TABLES.SCHEDULE, compileContentValues(courseObj), COLUMNS.ID + "=" + courseObj
				.getID(), null);
	}

	public int deleteCourse(final CourseObject courseObj) {
		if (courseObj.getID().equals("")) {
			Log.w(DAWGTAG, "SQL Error: course ID not defined");
			return -1;
		}
		return getReadableDatabase().delete(TABLES.SCHEDULE, COLUMNS.ID + "=" + courseObj.getID(), null);
	}

	public Cursor getScheduleCursor() {
		return getReadableDatabase().rawQuery("SELECT rowid _id,* FROM " + TABLES.SCHEDULE + " ORDER BY " + COLUMNS.COURSE_NAME + " ASC", null);
	}

	public List<CourseObject> getScheduleAsList() {
		return getScheduleAsList(null);
	}

	public List<CourseObject> getScheduleAsList(@Nullable final Day day) {
		final List<CourseObject> scheduleList = new ArrayList<>();

		final Cursor c;
		if (day == null) {
			c = getScheduleCursor();
		} else {
			c = getScheduleByDay(day);
		}

		if (c != null) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
				final CourseObject cObj = new CourseObject();
				cObj.setID(getCursorString(c, COLUMNS.ID));
				cObj.setCourseSubject(getCursorString(c, COLUMNS.COURSE_SUBJECT));
				cObj.setCourseNumber(getCursorString(c, COLUMNS.COURSE_NUMBER));
				cObj.setTitle(getCursorString(c, COLUMNS.COURSE_NAME));
				cObj.setBuilding(getCursorString(c, COLUMNS.COURSE_BUILDING));
				cObj.setRoomNumber(getCursorString(c, COLUMNS.COURSE_ROOM));
				cObj.setSectionNumber(getCursorString(c, COLUMNS.COURSE_SECTION));
				cObj.setCreditHours(getCursorString(c, COLUMNS.COURSE_CREDIT_HOURS));
				cObj.setInstructorName(getCursorString(c, COLUMNS.COURSE_INSTRUCTOR));

				cObj.setStartTime(new TimeObject(getCursorString(c, COLUMNS.COURSE_TIME_START)));
				cObj.setEndTime(new TimeObject(getCursorString(c, COLUMNS.COURSE_TIME_END)));

				final String courseDays = getCursorString(c, COLUMNS.COURSE_DAYS);

				if (courseDays.length() > 0) {
					final String[] split = courseDays.split(", ");

					for (final String s : split) {
						try {
							cObj.setDay(Day.valueOf(s.toUpperCase()), true);
						} catch (final Exception e) {
							e.printStackTrace();
							Log.e(DAWGTAG, "Something went wrong when loading Day for: " + cObj.getTitle());
						}
					}
				}

				scheduleList.add(cObj);
				c.moveToNext();
			}
			c.close();
		}
		try {
			Collections.sort(scheduleList);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return scheduleList;
	}

	public List<CourseObject> getScheduleAsListDayAndTime(final Day day, final TimeObject time) {
		final List<CourseObject> list = getScheduleAsList(day);
		final List<CourseObject> removeList = new ArrayList<>();
		for (final CourseObject cObj : list) {
			try {
				if (time.compareTo(cObj.getEndTime()) > 0) {
					removeList.add(cObj);
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		list.removeAll(removeList);
		return list;
	}

	public Cursor getScheduleByDay(final Day day) {
		// @formatter:off
		return getReadableDatabase().rawQuery(
				"SELECT rowid _id,* FROM " + TABLES.SCHEDULE +
				" WHERE " + COLUMNS.COURSE_DAYS + " LIKE '%" + day.toString() + "%'", null);
		// @formatter:on
	}

	private String getCursorString(final Cursor c, final String columnName) {
		return c.getString(c.getColumnIndex(columnName));
	}

	private ContentValues compileContentValues(final CourseObject courseObj) {
		final ContentValues values = new ContentValues();

		values.put(COLUMNS.COURSE_SUBJECT, courseObj.getCourseSubject());
		values.put(COLUMNS.COURSE_NUMBER, courseObj.getCourseNumber());
		values.put(COLUMNS.COURSE_SECTION, courseObj.getSectionNumber());
		values.put(COLUMNS.COURSE_NAME, courseObj.getTitle());
		values.put(COLUMNS.COURSE_CREDIT_HOURS, courseObj.getCreditHours());
		values.put(COLUMNS.COURSE_INSTRUCTOR, courseObj.getInstructorName());

		values.put(COLUMNS.COURSE_BUILDING, courseObj.getBuilding());
		values.put(COLUMNS.COURSE_ROOM, courseObj.getRoomNumber());

		values.put(COLUMNS.COURSE_TIME_START, courseObj.getSchedule().getStartTime().toString());
		values.put(COLUMNS.COURSE_TIME_END, courseObj.getSchedule().getEndTime().toString());
		values.put(COLUMNS.COURSE_DAYS, courseObj.getSchedule().getDayStringCommas());
		return values;
	}

	/**
	 * Exports a copy of the current database and copies it to a supplied Uri location
	 * @param uri the location of the backup
	 * @return true, if the export succeeded
	 */
	public boolean exportDatabaseToURI(final Uri uri, final ContentResolver contentResolver) {
		try {
			final ParcelFileDescriptor pfd = contentResolver.openFileDescriptor(uri, "w");
			final FileChannel destination = new FileOutputStream(pfd.getFileDescriptor()).getChannel();

			final File currentDB = new File(getReadableDatabase().getPath());
			final FileChannel source = new FileInputStream(currentDB).getChannel();

			destination.transferFrom(source, 0, source.size());

			source.close();
			destination.close();
			pfd.close();

			return true;
		} catch (final IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Deletes the current database and replaces it with an supplied database by Uri location
	 * @param uri the location of the backup to restore
	 * @return true, if the import succeeded
	 */
	public boolean importDatabaseFromURI(final Uri uri, final ContentResolver contentResolver) {
		try {
			final InputStream stream = contentResolver.openInputStream(uri);
			final ReadableByteChannel source = Channels.newChannel(stream);

			final File currentDB = new File(getReadableDatabase().getPath());
			final FileChannel destination = new FileOutputStream(currentDB).getChannel();

			destination.transferFrom(source, 0, stream.available());

			source.close();
			destination.close();
			stream.close();

			return true;
		} catch (final IOException e) {
			e.printStackTrace();
			return false;
		}
	}

}
