/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.database.object;

import java.io.Serializable;
import java.util.Calendar;

public class TimeObject implements Comparable<TimeObject>, Serializable {
	private int hourOfDay = 0;
	private int minute = 0;

	/**
	 * Creates a TimeObject from the current time
	 */
	public TimeObject() {
		final Calendar cal = Calendar.getInstance();
		this.hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
		this.minute = cal.get(Calendar.MINUTE);
	}

	/**
	 * Creates a TimeObject from 24 hour format
	 * @param hourOfDay the hours in 24 hour format
	 * @param minute the minutes
	 */
	public TimeObject(final int hourOfDay, final int minute) {
		this.hourOfDay = hourOfDay;
		this.minute = minute;

		if (this.hourOfDay > 24) {
			this.hourOfDay -= 24;
		}
	}

	/**
	 * Creates a TimeObject from 12 hour format
	 * @param hour the hours in 12 hour format
	 * @param minute the minutes
	 * @param isPM is the time in PM, not AM
	 */
	public TimeObject(final int hour, final int minute, final boolean isPM) {
		if (isPM && hour != 12) {
			this.hourOfDay = hour + 12;
		} else if (!isPM && hour == 12) {
			this.hourOfDay = 0;
		} else {
			this.hourOfDay = hour;
		}
		this.minute = minute;
		if (this.hourOfDay > 24) {
			this.hourOfDay -= 24;
		}
	}

	/**
	 * Creates a time object from the "HH:MM" (24 hour) format
	 * @param serialized the HH:MM string
	 */
	public TimeObject(final String serialized) {
		final String[] time = serialized.split(":");
		hourOfDay = Integer.valueOf(time[0]);
		minute = Integer.valueOf(time[1]);
		if (this.hourOfDay > 24) {
			this.hourOfDay -= 24;
		}
	}

	/**
	 * Gets the hours (24 hour format)
	 * @return the hours in 24 hour format
	 */
	public int getHourOfDay() {
		return hourOfDay;
	}

	/**
	 * Gets the hours (12 hour format)
	 * @return the hours in 12 hour format
	 */
	public int getHour() {
		if (hourOfDay == 0 || hourOfDay == 24) {
			return 12;
		} else if (hourOfDay > 12) {
			return hourOfDay - 12;
		}
		return hourOfDay;
	}

	/**
	 * Returns if the time is PM or AM (hourOfDay >= 12)
	 * @return
	 */
	public boolean isPM() {
		return hourOfDay >= 12;
	}

	/**
	 * Gets the stored minutes
	 * @return the minutes
	 */
	public int getMinute() {
		return minute;
	}

	/**
	 * Converts the TimeObject into "HH:MM" (24 hour) format
	 * @return "HH:MM"
	 */
	@Override
	public String toString() {
		return String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
	}

	/**
	 * Converts the TimeObject into "HH:MM aa" (12 hour) format
	 * @return "HH:MM aa"
	 */
	public String to12HourString() {
		return String.valueOf(getHour()) + ":" + String.format("%02d", minute) + (isPM() ? " PM" : " AM");
	}

	@Override
	public int compareTo(final TimeObject timeObj) {
		if (hourOfDay > timeObj.getHourOfDay()) {
			return 1;
		} else if (hourOfDay < timeObj.getHourOfDay()) {
			return -1;
		} else {
			if (minute > timeObj.getMinute()) {
				return 1;
			} else if (minute < timeObj.getMinute()) {
				return -1;
			}
		}
		return 0;
	}
}
