/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.frag;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.Constants;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;
import com.mapbox.services.directions.v4.DirectionsCriteria;
import com.mapbox.services.directions.v4.MapboxDirections;
import com.mapbox.services.directions.v4.models.DirectionsResponse;
import com.mapbox.services.directions.v4.models.DirectionsRoute;
import com.mapbox.services.directions.v4.models.Waypoint;
import com.operontech.maroon.R;
import com.operontech.maroon.database.object.PlaceObject;
import com.operontech.maroon.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static com.mapbox.mapboxsdk.constants.MapboxConstants.TAG;
import static com.operontech.maroon.MainActivity.DAWGTAG;

public class FragmentMap extends Fragment {

	final int PERMISSION_REQUEST_CODE = 1;
	SharedPreferences mSharedPreferences;
	MapboxMap mMap;
	MapView mapView;
	LocationServices locServices;
	DirectionsRoute currentRoute;
	boolean viewLocation = true;
	boolean hasRoute = false;

	NestedScrollView bottomSheet;
	TextView bsTitleText;
	TextView bsDescriptionText;

	@Override
	public void onCreate(final Bundle bundle) {
		super.onCreate(bundle);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_map, container, false);
		locServices = LocationServices.getLocationServices(inflater.getContext());
		mapView = (MapView) root.findViewById(R.id.map);
		bottomSheet = (NestedScrollView) root.findViewById(R.id.map_bottom_sheet);
		bsTitleText = (TextView) bottomSheet.findViewById(R.id.bottomsheet_text_title);
		bsDescriptionText = (TextView) bottomSheet.findViewById(R.id.bottomsheet_text_description);

		try {
			mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
			switch (mSharedPreferences.getString("mapType", "streets")) {
				case ("streets"):
					mapView.setStyleUrl(Style.MAPBOX_STREETS);
					break;
				case ("satellite"):
					mapView.setStyleUrl(Style.SATELLITE);
					break;
				case ("satellite + streets"):
					mapView.setStyleUrl(Style.SATELLITE_STREETS);
					break;
				case ("outdoors"):
					mapView.setStyleUrl(Style.OUTDOORS);
					break;
				case ("light"):
					mapView.setStyleUrl(Style.LIGHT);
					break;
				case ("dark"):
					mapView.setStyleUrl(Style.DARK);
					break;
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

		mapView.onCreate(savedInstanceState);
		mapView.getMapAsync(new OnMapReadyCallback() {
			@Override
			public void onMapReady(final MapboxMap mapboxMap) {
				mMap = mapboxMap;

				// Set the theme/design
				mMap.getMyLocationViewSettings().setAccuracyAlpha(0);

				setupMap(true);
			}
		});
		setHasOptionsMenu(true);
		return root;
	}

	private void setupMap(final boolean camera) {
		// Runs routing in a thread to reduce lag on main thread
		new Thread() {
			@Override
			public void run() {
				final Bundle args = getArguments();
				final boolean successGPS = setupGPS();
				final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
				final int px80 = Util.dpToPx(80);
				hasRoute = args != null;

				// If the camera should move and GPS was found, move the map
				if (camera && successGPS) {
					setDefaultMap();
				}

				if (hasRoute) {
					viewLocation = false;

					// Set bottomSheet to visible and cut the mapView short
					bottomSheet.setVisibility(View.VISIBLE);
					params.setMargins(0, 0, 0, px80);
					mapView.setLayoutParams(params);

					setPlaceMap(camera, (PlaceObject) args.getSerializable("place"));
					return;
				} else if (successGPS) {
					// Hide bottomSheet and reset mapView margins
					params.setMargins(0, 0, 0, 0);
					mapView.setLayoutParams(params);
					bottomSheet.setVisibility(View.INVISIBLE);
				} else {
					// Hide bottomSheet and reset mapView margins
					params.setMargins(0, 0, 0, 0);
					mapView.setLayoutParams(params);
					bottomSheet.setVisibility(View.INVISIBLE);

					// Make a place named Mississippi State University
					setPlaceMap(camera, new PlaceObject("Mississippi State University", "", new LatLng(33.4537233, -88.7902384)));
				}

				mMap.getUiSettings().setAttributionGravity(Gravity.BOTTOM | Gravity.START);
			}
		}.run();
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isAdded()) {
			return;
		}
		getActivity().setTitle(getString(R.string.type_campus_map));
		if (currentRoute == null && hasRoute) {
			setupMap(false);
		}
		if (mapView != null) {
			mapView.onResume();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mapView != null) {
			mapView.onPause();
		}
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		if (mapView != null) {
			mapView.onLowMemory();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mapView != null) {
			mapView.onDestroy();
		}
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("hasRoute", hasRoute);

		if (mapView != null && outState != null) {
			mapView.onSaveInstanceState(outState);
		}
	}

	@Override
	public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
		inflater.inflate(R.menu.menu_location, menu);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		if (item.getItemId() == R.id.action_location) {
			animToUserLocation();
		}
		return true;
	}

	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
		if (requestCode == PERMISSION_REQUEST_CODE) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				setupGPS();
				animToUserLocation();
			} else {
				Toast.makeText(getActivity(), "Please enable location for a better experience", Toast.LENGTH_LONG)
				     .show();
			}
		}
	}

	/**
	 * Sets the camera to the user's position (no animation)
	 */
	public void setDefaultMap() {
		final Location lastLocation = locServices.getLastLocation();
		if (lastLocation != null) {
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 17.5f));
		}
	}

	/**
	 * Sets a waypoint and gets directions to a place
	 * @param camera
	 * @param place the destination
	 */
	public void setPlaceMap(final boolean camera, final PlaceObject place) {
		if (mMap != null && camera) {
			mMap.addMarker(new MarkerViewOptions().position(place.getLatLng())
			                                      .title(place.getName())
			                                      .snippet(place.getContext()));
		}
		getRoute(place);
	}

	/**
	 * Gets and sets the route on the map
	 * @param place the destination
	 */
	private void getRoute(final PlaceObject place) {
		final Location lastLocation = locServices.getLastLocation();
		if (lastLocation != null) {
			try {
				final MapboxDirections.Builder builder = new MapboxDirections.Builder();
				builder.setAccessToken(MapboxAccountManager.getInstance().getAccessToken());
				builder.setOrigin(new Waypoint(lastLocation.getLongitude(), lastLocation.getLatitude()));
				builder.setDestination(new Waypoint(place.getLongitude(), place.getLatitude()));

				try {
					switch (mSharedPreferences.getString("navigationMode", "walking")) {
						case "walking":
							builder.setProfile(DirectionsCriteria.PROFILE_WALKING);
							break;
						case "cycling":
							builder.setProfile(DirectionsCriteria.PROFILE_CYCLING);
							break;
						case "driving":
							builder.setProfile(DirectionsCriteria.PROFILE_DRIVING);
							break;
						default:
							builder.setProfile(DirectionsCriteria.PROFILE_WALKING);
							break;
					}
				} catch (final Exception e) {
					builder.setProfile(DirectionsCriteria.PROFILE_WALKING);
					e.printStackTrace();
					Log.e(DAWGTAG, "Preference Error: navigationMode | defaulting to 'walking'");
				}

				final MapboxDirections client = builder.build();
				client.enqueueCall(new Callback<DirectionsResponse>() {
					@Override
					public void onResponse(final Call<DirectionsResponse> call, final Response<DirectionsResponse> response) {

						// Ensure a response was received
						if (response.body() == null) {
							Log.e(DAWGTAG, "Error: No route was received!");
							return;
						} else if (response.body().getRoutes().size() < 1) {
							Log.e(DAWGTAG, "Error: No route was received!");
							return;
						}

						// Get the currentRout then
						currentRoute = response.body().getRoutes().get(0);
						final int duration = Math.round(currentRoute.getDuration() / 60);

						bsTitleText.setText(place.getName());
						//@formatter:off
						bsDescriptionText.setText(getResources().getString(R.string.nav_travel_time,
																			duration,
																			(duration > 1 ? " minutes" : " minute"),
																			currentRoute.getDistance()));
						//@formatter:on
						bottomSheet.setVisibility(View.VISIBLE);

						// Draw the route
						drawRoute(currentRoute);

						// Create a bounding box that contains the positions of both the start and end
						final LatLngBounds latLngBounds = new LatLngBounds.Builder().include(new LatLng(lastLocation))
						                                                            .include(place.getLatLng())
						                                                            .build();

						// Animate camera to that new bouding box
						mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 250), 2000);
					}

					@Override
					public void onFailure(final Call<DirectionsResponse> call, final Throwable throwable) {
						Log.e(TAG, "Error: " + throwable.getMessage());
						Toast.makeText(getActivity(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).
								show();
					}
				});
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Draws a route on the map using Polylines
	 * @param route the route to draw
	 */
	private void drawRoute(final DirectionsRoute route) {
		final LineString lineString = LineString.fromPolyline(route.getGeometry(), Constants.OSRM_PRECISION_V4);
		final List<Position> coordinates = lineString.getCoordinates();
		final LatLng[] points = new LatLng[coordinates.size()];
		for (int i = 0; i < coordinates.size(); i++) {
			points[i] = new LatLng(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude());
		}
		if (getContext() != null) {
			mMap.addPolyline(new PolylineOptions().add(points)
			                                      .color(ContextCompat.getColor(getContext(), R.color.colorAccent))
			                                      .width(6));
		}
	}

	/**
	 * Initialises LocationServices and enables location
	 * @return if permissions were granted
	 */
	private boolean setupGPS() {
		if (!locServices.areLocationPermissionsGranted()) {
			ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_REQUEST_CODE);
			return false;
		} else {
			locServices.addLocationListener(new LocationListener() {
				@Override
				public void onLocationChanged(final Location location) {
					if (!viewLocation) {
						locServices.removeLocationListener(this);
					} else if (location != null) {
						mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 17.5f));
						locServices.removeLocationListener(this);
					}
				}
			});
			mMap.setMyLocationEnabled(true);
			locServices.toggleGPS(true);
			return true;
		}
	}

	/**
	 * Animate the camera to the user's location
	 */
	private void animToUserLocation() {
		final Location loc = locServices.getLastLocation();
		if (loc != null) {
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc), 17.5f), 2000);
		}
	}
}
