/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.frag;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.*;
import com.operontech.maroon.R;
import com.operontech.maroon.adapter.PlacesRecyclerAdapter;
import com.operontech.maroon.async.PlacesAsync;
import com.operontech.maroon.database.object.PlaceObject;
import com.operontech.maroon.util.RecyclerItemClickListener;

import java.util.List;

public class FragmentPlaces extends Fragment {

	private RecyclerView recyclerView;
	private PlacesRecyclerAdapter placesAdapter;
	FragmentManager fManager;

	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_places, container, false);
		fManager = getActivity().getSupportFragmentManager();
		return root;
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		recyclerView = (RecyclerView) getActivity().findViewById(R.id.places_list);
		recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
			@Override
			public void onItemClick(final View view, final int position) {
				final FragmentMap fragMap = new FragmentMap();
				final Bundle args = new Bundle();
				final PlaceObject p = placesAdapter.getItem(position);

				args.putSerializable("place", p);
				fragMap.setArguments(args);

				//@formatter:off
				fManager.beginTransaction()
				        .replace(R.id.frag, fragMap)
				        .addToBackStack(null)
				        .commit();
				//@formatter:on
			}

			@Override
			public void onLongItemClick(final View view, final int position) {

			}
		}));

		final PlacesAsync task = new PlacesAsync(this, getArguments().getString("target"));
		task.execute();

	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isAdded()) {
			return;
		}
		getActivity().setTitle(getArguments().getString("targetPlural"));
	}

	@Override
	public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
		inflater.inflate(R.menu.menu_search, menu);

		final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(final String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(final String newText) {
				placesAdapter.getFilter().filter(newText);
				return true;
			}
		});
	}

	/**
	 * Sets the list to use for the RecyclerAdapter
	 * This is called when the AsyncTask completes
	 */
	public void setupAdapter(final List<PlaceObject> list) {
		if (placesAdapter != null) {
			placesAdapter.swapList(list);
		} else {
			placesAdapter = new PlacesRecyclerAdapter(list);
		}
		recyclerView.setAdapter(placesAdapter);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		recyclerView.setHasFixedSize(true);
	}
}
