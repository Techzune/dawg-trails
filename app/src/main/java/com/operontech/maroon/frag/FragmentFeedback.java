/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.frag;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.operontech.maroon.R;

public class FragmentFeedback extends Fragment {

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_feedback, container, false);

		root.findViewById(R.id.submit_feedback_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				final Intent feedBackIntent = new Intent(Intent.ACTION_SEND);
				feedBackIntent.setType("text/html");
				feedBackIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { getString(R.string.mail_feedback_email) });
				feedBackIntent.putExtra(Intent.EXTRA_SUBJECT, "App Feedback");
				feedBackIntent.putExtra(Intent.EXTRA_TEXT, ((EditText) root.findViewById(R.id.comment_text)).getText());
				startActivity(Intent.createChooser(feedBackIntent, "Send feedback:"));
			}
		});

		return root;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isAdded()) {
			return;
		}
		getActivity().setTitle("Feedback");
	}
}
