/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.frag;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.Toast;
import com.operontech.maroon.R;
import com.operontech.maroon.activity.ActivityCourseEdit;
import com.operontech.maroon.adapter.ScheduleRecyclerAdapter;
import com.operontech.maroon.database.ScheduleDatabase;
import com.operontech.maroon.database.object.CourseObject;
import com.operontech.maroon.util.RecyclerItemClickListener;
import com.operontech.maroon.util.Util;

import java.util.List;

public class FragmentCourses extends Fragment {
	private final int EXPORT_COURSES_REQUEST = 101;
	private final int IMPORT_COURSES_REQUEST = 102;

	private ScheduleDatabase db;
	private RecyclerView recyclerView;
	private FloatingActionButton fab;
	private ScheduleRecyclerAdapter scheduleAdapter;
	FragmentManager fManager;

	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_courses, container, false);

		fManager = getActivity().getSupportFragmentManager();
		fManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {
				if (fManager.getBackStackEntryCount() == 0) {
					onResume();
				}
			}
		});

		recyclerView = (RecyclerView) root.findViewById(R.id.courses_recycler_schedule);
		recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
			@Override
			public void onItemClick(final View view, final int position) {
				final CourseObject cObj = scheduleAdapter.getItem(position);

				final Intent intent = new Intent(getActivity(), ActivityCourseEdit.class);
				intent.putExtra("course", cObj);
				startActivity(intent);
			}

			@Override
			public void onLongItemClick(final View view, final int position) {

			}
		}));

		fab = (FloatingActionButton) root.findViewById(R.id.courses_fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				final Intent intent = new Intent(getActivity(), ActivityCourseEdit.class);
				intent.putExtra("course", new CourseObject());
				startActivity(intent);

				setupAdapter(db.getScheduleAsList());
			}
		});

		return root;
	}

	@Override
	public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		db = ScheduleDatabase.getInstance(getContext().getApplicationContext());
		setupAdapter(db.getScheduleAsList());
	}

	@Override
	public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
		inflater.inflate(R.menu.menu_courses, menu);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_export_courses:
				requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE }, EXPORT_COURSES_REQUEST);
				return true;
			case R.id.action_import_courses:
				requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE }, IMPORT_COURSES_REQUEST);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
		if (requestCode == EXPORT_COURSES_REQUEST) {
			// Export File
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				try {
					final Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
					intent.addCategory(Intent.CATEGORY_OPENABLE);
					intent.setType("application/dawgtrails");
					intent.putExtra(Intent.EXTRA_TITLE, Util.createFileTimeStamp() + ".dawgbase");
					startActivityForResult(intent, EXPORT_COURSES_REQUEST);
				} catch (final Exception e) {
					e.printStackTrace();
					Toast.makeText(getActivity(), "Failed: no storage application found", Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(getActivity(), "Failed: storage permissions not granted", Toast.LENGTH_SHORT).show();
			}
		} else if (requestCode == IMPORT_COURSES_REQUEST) {
			// Import File
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				try {
					final Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
					intent.setType("*/*");
					startActivityForResult(intent, IMPORT_COURSES_REQUEST);
				} catch (final Exception e) {
					e.printStackTrace();
					Toast.makeText(getActivity(), "Failed: no storage application found", Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(getActivity(), "Failed: storage permissions not granted", Toast.LENGTH_SHORT).show();
			}
		} else {
			// None of the Above
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent resultData) {
		if (requestCode == EXPORT_COURSES_REQUEST && resultCode == Activity.RESULT_OK) {
			if (resultData != null) {
				final boolean result = db.exportDatabaseToURI(resultData.getData(), getActivity().getContentResolver());
				Toast.makeText(getContext(), result ? "Export succeeded!" : "Export failed...", Toast.LENGTH_SHORT)
				     .show();
			}
		} else if (requestCode == IMPORT_COURSES_REQUEST && resultCode == Activity.RESULT_OK) {
			if (resultData != null) {
				// Verify that it actually is an appropriate database file
				if (!verifyFileExtension(resultData.getData())) {
					Toast.makeText(getContext(), "Error: Not a valid database backup file", Toast.LENGTH_LONG).show();
					return;
				}

				final boolean result = db.importDatabaseFromURI(resultData.getData(), getActivity().getContentResolver());
				Toast.makeText(getContext(), result ? "Restore succeeded!" : "Restore failed...", Toast.LENGTH_SHORT)
				     .show();
			}
		} else {
			super.onActivityResult(requestCode, resultCode, resultData);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isAdded()) {
			return;
		}
		getActivity().setTitle("My Courses");
		setupAdapter(db.getScheduleAsList());
	}

	@Override
	public void onDestroy() {
		if (db != null) {
			db.close();
		}
		super.onDestroy();
	}

	public void setupAdapter(final List<CourseObject> list) {
		if (scheduleAdapter != null) {
			scheduleAdapter.swapList(list);
		} else {
			scheduleAdapter = new ScheduleRecyclerAdapter(list);
		}
		recyclerView.setAdapter(scheduleAdapter);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		recyclerView.setHasFixedSize(true);
	}

	public boolean verifyFileExtension(final Uri uri) {
		final String uriString = uri.toString();
		final String extension = uriString.substring(uriString.lastIndexOf('.')).split("%20")[0];
		return extension.equalsIgnoreCase(".dawgbase");
	}
}
