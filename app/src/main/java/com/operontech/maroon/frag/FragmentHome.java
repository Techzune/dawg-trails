/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.frag;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.operontech.maroon.R;
import com.operontech.maroon.adapter.ScheduleRecyclerAdapter;
import com.operontech.maroon.database.ScheduleDatabase;
import com.operontech.maroon.database.object.CourseObject;
import com.operontech.maroon.database.object.Day;
import com.operontech.maroon.database.object.TimeObject;

import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;

public class FragmentHome extends Fragment {
	TextView timeOfDayGreeting;
	private ScheduleDatabase db;
	private RecyclerView recyclerView;
	private ScheduleRecyclerAdapter scheduleAdapter;
	private RelativeLayout clearDayView;
	FragmentManager fManager;

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_home, container, false);

		fManager = getActivity().getSupportFragmentManager();
		fManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {
				if (fManager.getBackStackEntryCount() == 0) {
					onResume();
				}
			}
		});

		timeOfDayGreeting = (TextView) root.findViewById(R.id.home_greeting_text);
		recyclerView = (RecyclerView) root.findViewById(R.id.home_recycler_schedule);
		clearDayView = (RelativeLayout) root.findViewById(R.id.home_clear_day_view);
		return root;
	}

	@Override
	public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		db = ScheduleDatabase.getInstance(getContext().getApplicationContext());
		setTimeOfDayGreeting();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isAdded()) {
			return;
		}
		getActivity().setTitle("Home");
		setTimeOfDayGreeting();
	}

	@Override
	public void onDestroy() {
		if (db != null) {
			db.close();
		}
		super.onDestroy();
	}

	/**
	 * Changed the TimeOfDay TextView to the appropriate message for the current day+time
	 */
	private void setTimeOfDayGreeting() {
		try {
			int greetingID = R.string.greeting_morning;
			final Date dt = new Date();
			final int hours = dt.getHours();

			if (hours >= 0 && hours < 12) {
				greetingID = R.string.greeting_morning;
			} else if (hours >= 12 && hours < 16) {
				greetingID = R.string.greeting_afternoon;
			} else if (hours >= 16 && hours <= 24) {
				greetingID = R.string.greeting_evening;
			}

			final String dayString = DateFormat.format("EEEE", dt).toString();
			timeOfDayGreeting.setText(getString(greetingID, dayString));

			final Day dayEnum = Day.fromName(dayString);
			setupAdapter(db.getScheduleAsListDayAndTime(dayEnum, new TimeObject()));
		} catch (final ConcurrentModificationException cme) {
			cme.printStackTrace();
			Toast.makeText(getContext(), "An error occurred while displaying the schedule!", Toast.LENGTH_LONG).show();
		} catch (final Exception e) {
			e.printStackTrace();
			Toast.makeText(getContext(), "An error occurred when setting the day!", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Sets the adapter for the Schedule RecyclerView
	 * @param list the list of courses to display
	 */
	public void setupAdapter(final List<CourseObject> list) {
		// If list is empty, display clear day message and return
		// otherwise, hide the message and continue
		if (list.size() == 0) {
			clearDayView.setVisibility(View.VISIBLE);
			recyclerView.setVisibility(View.GONE);

			// Clear the data from the scheduleAdapter to save on memory
			if (scheduleAdapter != null) {
				scheduleAdapter.clearAllItems();
			}
			return;
		}

		clearDayView.setVisibility(View.GONE);
		recyclerView.setVisibility(View.VISIBLE);

		try {
			if (scheduleAdapter != null) {
				scheduleAdapter.swapList(list);
			} else {
				scheduleAdapter = new ScheduleRecyclerAdapter(list);
			}
			recyclerView.setAdapter(scheduleAdapter);
			recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
			recyclerView.setHasFixedSize(true);
		} catch (final Exception e) {
			e.printStackTrace();
			Toast.makeText(getActivity(), "SQL Database Error", Toast.LENGTH_LONG).show();
		}
	}
}
