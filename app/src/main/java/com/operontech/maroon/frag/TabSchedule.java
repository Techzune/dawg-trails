/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.frag;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.operontech.maroon.R;
import com.operontech.maroon.activity.ActivityCourseEdit;
import com.operontech.maroon.adapter.ScheduleRecyclerAdapter;
import com.operontech.maroon.database.ScheduleDatabase;
import com.operontech.maroon.database.object.CourseObject;
import com.operontech.maroon.database.object.Day;
import com.operontech.maroon.util.RecyclerItemClickListener;

import java.util.List;

public class TabSchedule extends Fragment {
	private ScheduleDatabase db;
	private RecyclerView recyclerView;
	private ScheduleRecyclerAdapter scheduleAdapter;

	@Nullable
	@Override
	public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
		final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.page_schedule, container, false);

		recyclerView = (RecyclerView) root.findViewById(R.id.schedule_recycler_schedule);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		recyclerView.setHasFixedSize(true);

		recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
			@Override
			public void onItemClick(final View view, final int position) {
				final CourseObject cObj = scheduleAdapter.getItem(position);

				final Intent intent = new Intent(getActivity(), ActivityCourseEdit.class);
				intent.putExtra("course", cObj);
				startActivity(intent);
			}

			@Override
			public void onLongItemClick(final View view, final int position) {

			}
		}));

		return root;
	}

	@Override
	public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		db = ScheduleDatabase.getInstance(getContext().getApplicationContext());
		setupAdapter(db.getScheduleAsList(Day.valueOf(getArguments().getString("day"))));
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!isAdded()) {
			return;
		}
		setupAdapter(db.getScheduleAsList(Day.valueOf(getArguments().getString("day"))));
	}

	@Override
	public void onDestroy() {
		if (db != null) {
			db.close();
		}
		super.onDestroy();
	}

	public void setupAdapter(final List<CourseObject> list) {
		if (scheduleAdapter != null) {
			scheduleAdapter.swapList(list);
		} else {
			scheduleAdapter = new ScheduleRecyclerAdapter(list);
		}
		recyclerView.setAdapter(scheduleAdapter);
	}
}
