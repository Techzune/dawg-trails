/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.operontech.maroon.R;
import com.operontech.maroon.database.object.CourseObject;

import java.util.List;

public class ScheduleRecyclerAdapter extends RecyclerView.Adapter<ScheduleRecyclerAdapter.CourseViewHolder> {
	private List<CourseObject> courseList;

	public ScheduleRecyclerAdapter(final List<CourseObject> items) {
		courseList = items;
	}

	@Override
	public CourseViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_schedule, parent, false);

		return new CourseViewHolder(v);
	}

	@Override
	public void onBindViewHolder(final CourseViewHolder holder, final int position) {
		final CourseObject cObj = courseList.get(position);

		holder.courseName.setText(cObj.getTitle());
		holder.courseInfo.setText(cObj.getCourseSubject() + " " + cObj.getCourseNumber());
		holder.courseTimes.setText(cObj.getStartTime().to12HourString().replace(" ", "") + " - " + cObj.getEndTime()
		                                                                                               .to12HourString()
		                                                                                               .replace(" ", ""));
		holder.courseLocation.setText(cObj.getBuilding() + " " + cObj.getRoomNumber());
	}

	@Override
	public int getItemCount() {
		return courseList.size();
	}

	public CourseObject getItem(final int position) {
		return courseList.get(position);
	}

	/**
	 * Removes all items in the Course list
	 */
	public void clearAllItems() {
		courseList.clear();
		notifyDataSetChanged();
	}

	/**
	 * Swaps the current list with a new list and updates the adapter
	 * @param newList the new list to use
	 */
	public void swapList(final List<CourseObject> newList) {
		courseList = newList;
		notifyDataSetChanged();
	}

	public static class CourseViewHolder extends RecyclerView.ViewHolder {
		TextView courseName;
		TextView courseInfo;
		TextView courseTimes;
		TextView courseLocation;

		public CourseViewHolder(final View view) {
			super(view);
			courseName = (TextView) view.findViewById(R.id.item_place_name);
			courseInfo = (TextView) view.findViewById(R.id.item_place_description);
			courseTimes = (TextView) view.findViewById(R.id.item_schedule_coursetimes);
			courseLocation = (TextView) view.findViewById(R.id.item_schedule_courselocation);
		}
	}

}
