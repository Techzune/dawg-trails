/*
 * Copyright (c) 2017 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.operontech.maroon.R;
import com.operontech.maroon.database.object.PlaceObject;

import java.util.ArrayList;
import java.util.List;

public class PlacesRecyclerAdapter extends RecyclerView.Adapter<PlacesRecyclerAdapter.PlaceViewHolder> implements Filterable {
	List<PlaceObject> origPlaces;
	List<PlaceObject> tempPlaces;

	public PlacesRecyclerAdapter(final List<PlaceObject> places) {
		origPlaces = places;
		tempPlaces = places;
	}

	@Override
	public PlaceViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_place, parent, false);

		return new PlaceViewHolder(v);
	}

	@Override
	public void onBindViewHolder(final PlaceViewHolder holder, final int position) {
		final PlaceObject pObj = tempPlaces.get(position);
		holder.placeName.setText(pObj.getName());

		final String description = pObj.getDescription().toLowerCase();
		if (description.replaceAll(" ", "").length() > 0) {
			holder.placeInfo.setText(pObj.getDescription().toLowerCase());
			holder.placeInfo.setVisibility(View.VISIBLE);
		} else {
			holder.placeInfo.setVisibility(View.GONE);
		}
	}

	@Override
	public int getItemCount() {
		return tempPlaces.size();
	}

	/**
	 * Gets the PlaceObject from an item's position in the list
	 * @param position the position id of the item
	 * @return the PlaceObject for that item
	 */
	public PlaceObject getItem(final int position) {
		return tempPlaces.get(position);
	}

	/**
	 * Swaps the current list with a new list and updates the adapter
	 * @param newList the new list to use
	 */
	public void swapList(final List<PlaceObject> newList) {
		origPlaces = newList;
		tempPlaces = newList;
		notifyDataSetChanged();
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(final CharSequence constraint, final FilterResults results) {
				tempPlaces = (ArrayList<PlaceObject>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(final CharSequence constraint) {
				final FilterResults results = new FilterResults();
				final ArrayList<PlaceObject> FilteredList = new ArrayList<>();
				if (constraint == null || constraint.length() == 0) {
					results.values = origPlaces;
					results.count = origPlaces.size();
				} else {
					for (int i = 0; i < origPlaces.size(); i++) {
						final PlaceObject data = origPlaces.get(i);
						if (data.getName().toLowerCase().contains(constraint.toString())) {
							FilteredList.add(data);
						}
					}
					results.values = FilteredList;
					results.count = FilteredList.size();
				}
				return results;
			}
		};
	}

	public static class PlaceViewHolder extends RecyclerView.ViewHolder {
		TextView placeName;
		TextView placeInfo;

		public PlaceViewHolder(final View view) {
			super(view);
			placeName = (TextView) view.findViewById(R.id.item_place_name);
			placeInfo = (TextView) view.findViewById(R.id.item_place_description);
		}
	}

}
