/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;
import com.operontech.maroon.database.object.Day;
import com.operontech.maroon.frag.TabSchedule;

import static com.operontech.maroon.MainActivity.DAWGTAG;

public class SchedulePagesAdapter extends FragmentStatePagerAdapter {

	TabSchedule currentTab;

	public SchedulePagesAdapter(final FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(final int position) {

		switch (position) {
			case 0:
				return getNewTab(Day.SUNDAY);
			case 1:
				return getNewTab(Day.MONDAY);
			case 2:
				return getNewTab(Day.TUESDAY);
			case 3:
				return getNewTab(Day.WEDNESDAY);
			case 4:
				return getNewTab(Day.THURSDAY);
			case 5:
				return getNewTab(Day.FRIDAY);
			case 6:
				return getNewTab(Day.SATURDAY);
			default:
				Log.e(DAWGTAG, "Improper page ID: " + String.valueOf(position));
				return getNewTab(Day.SUNDAY);
		}
	}

	@Override
	public int getCount() {
		return 7;
	}

	@Override
	public CharSequence getPageTitle(final int position) {
		String title = "";
		switch (position) {
			case 0:
				title = "SUN";
				break;
			case 1:
				title = "MON";
				break;
			case 2:
				title = "TUE";
				break;
			case 3:
				title = "WED";
				break;
			case 4:
				title = "THU";
				break;
			case 5:
				title = "FRI";
				break;
			case 6:
				title = "SAT";
				break;
		}
		return title;
	}

	@Override
	public void setPrimaryItem(final ViewGroup container, final int position, final Object object) {
		if (getCurrentTab() != object) {
			currentTab = ((TabSchedule) object);
		}
		super.setPrimaryItem(container, position, object);
	}

	public TabSchedule getCurrentTab() {
		return currentTab;
	}

	/**
	 * Creates a TabSchedule based on a Day enum
	 * @param day the Day to base on
	 * @return the new TabSchedule
	 */
	private TabSchedule getNewTab(final Day day) {
		final Bundle args = new Bundle();
		args.putString("day", day.toString());

		final TabSchedule tab = new TabSchedule();
		tab.setArguments(args);

		return tab;
	}
}
