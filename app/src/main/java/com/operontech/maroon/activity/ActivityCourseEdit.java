package com.operontech.maroon.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.operontech.maroon.R;
import com.operontech.maroon.database.ScheduleDatabase;
import com.operontech.maroon.database.object.CourseObject;
import com.operontech.maroon.database.object.Day;
import com.operontech.maroon.database.object.TimeObject;
import com.operontech.maroon.util.Util;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.operontech.maroon.MainActivity.DAWGTAG;

public class ActivityCourseEdit extends AppCompatActivity {
	Toolbar toolbar;
	ArrayAdapter spinnerAdapter;
	EditText courseTitle;
	Spinner courseSubject;
	EditText courseNumber;
	EditText courseSection;
	TextView instructorText;

	TextView startTimeText;
	TextView endTimeText;
	TextView locationText;
	TextView daysText;
	Button deleteButton;

	CourseObject courseObj;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_edit);
		final Bundle args = getIntent().getExtras();

		if (args != null && args.containsKey("course")) {
			courseObj = (CourseObject) args.getSerializable("course");
		} else {
			courseObj = new CourseObject();
		}

		toolbar = (Toolbar) findViewById(R.id.course_edit_toolbar);
		courseTitle = (EditText) findViewById(R.id.course_edit_courseTitle);
		courseSubject = (Spinner) findViewById(R.id.course_edit_subject);
		courseNumber = (EditText) findViewById(R.id.course_edit_courseNum);
		courseSection = (EditText) findViewById(R.id.course_edit_courseSec);
		instructorText = (TextView) findViewById(R.id.course_edit_instructorName);

		startTimeText = (TextView) findViewById(R.id.course_edit_startTime);
		endTimeText = (TextView) findViewById(R.id.course_edit_endTime);
		locationText = (TextView) findViewById(R.id.course_edit_locationText);
		daysText = (TextView) findViewById(R.id.course_edit_daysText);

		deleteButton = (Button) findViewById(R.id.course_edit_delete);

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

		spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.course_types, R.layout.spinner_course_edit_subject);
		courseSubject.setAdapter(spinnerAdapter);

		final ArrayList<String> list = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.course_types)));
		final int pos = list.indexOf(courseObj.getCourseSubject());
		if (pos != -1) {
			courseSubject.setSelection(pos);
		}

		courseTitle.setOnFocusChangeListener(new EditTextFocusChangeStopEditing());
		setCourseString(courseTitle, courseObj.getTitle());

		courseNumber.setOnFocusChangeListener(new EditTextFocusChangeStopEditing());
		setCourseString(courseNumber, courseObj.getCourseNumber());

		courseSection.setOnFocusChangeListener(new EditTextFocusChangeStopEditing());
		setCourseString(courseSection, courseObj.getSectionNumber());

		setCourseString(instructorText, courseObj.getInstructorName());

		setCourseString(locationText, courseObj.getBuilding() + " " + courseObj.getRoomNumber());

		setCourseString(daysText, courseObj.getSchedule().getDayStringCommasAbrv());

		instructorText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				final LayoutInflater inflater = getLayoutInflater();
				final View dialogContent = inflater.inflate(R.layout.dialog_course_edit_instructor, null);
				final TextView instructorInput = (TextView) dialogContent.findViewById(R.id.instructor_edit_input);
				//@formatter:off
				new AlertDialog.Builder(ActivityCourseEdit.this)
						.setTitle("Set Instructor Name")
						.setView(dialogContent)
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialogInterface, final int i) {
								courseObj.setInstructorName(instructorInput.getText().toString());
								setCourseString(instructorText, courseObj.getInstructorName());
							}})
						.setNegativeButton("Cancel", null)
						.show();
				//@formatter:on
				instructorInput.setText(courseObj.getInstructorName());
			}
		});

		updateTimeText();

		startTimeText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				createTimeDialog("TimePickerDialogStart", courseObj.getStartTime(), new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(final RadialPickerLayout view, final int hourOfDay, final int minute, final int second) {
						if (courseObj.getEndTime().compareTo(new TimeObject(hourOfDay, minute)) <= 0) {
							courseObj.setStartTime(new TimeObject(hourOfDay, minute));
							courseObj.setEndTime(new TimeObject(hourOfDay + 1, minute));
						} else {
							courseObj.setStartTime(new TimeObject(hourOfDay, minute));
						}
						updateTimeText();
					}
				});
			}
		});

		endTimeText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				createTimeDialog("TimePickerDialogEnd", courseObj.getEndTime(), new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(final RadialPickerLayout view, final int hourOfDay, final int minute, final int second) {
						if (courseObj.getStartTime().compareTo(new TimeObject(hourOfDay, minute)) >= 0) {
							courseObj.setStartTime(new TimeObject(hourOfDay, minute));
							courseObj.setEndTime(new TimeObject(hourOfDay + 1, minute));
						} else {
							courseObj.setEndTime(new TimeObject(hourOfDay, minute));
						}
						updateTimeText();
					}
				});
			}
		});

		locationText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				final LayoutInflater inflater = getLayoutInflater();
				final View dialogContent = inflater.inflate(R.layout.dialog_course_edit_location, null);
				final TextView bName = (TextView) dialogContent.findViewById(R.id.location_edit_buildingName);
				final TextView rNumber = (TextView) dialogContent.findViewById(R.id.location_edit_roomNumber);
				//@formatter:off
				new AlertDialog.Builder(ActivityCourseEdit.this)
						.setTitle("Set Course Location")
						.setView(dialogContent)
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialogInterface, final int i) {
								courseObj.setBuilding(bName.getText().toString());
								courseObj.setRoomNumber(rNumber.getText().toString());
								setCourseString(locationText, courseObj.getBuilding() + " " + courseObj.getRoomNumber());
							}})
						.setNegativeButton("Cancel", null)
						.show();
				//@formatter:on
				bName.setText(courseObj.getBuilding());
				rNumber.setText(courseObj.getRoomNumber());
			}
		});

		daysText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				final boolean[] daysCheck = { false, false, false, false, false, false, false };
				final List<Integer> selectedDays = new ArrayList<>();

				for (final Map.Entry<Day, Boolean> entry : courseObj.getSchedule().getDaysOfTheWeek().entrySet()) {
					if (entry.getValue()) {
						daysCheck[entry.getKey().getIndex()] = true;
						selectedDays.add(entry.getKey().getIndex());
					}
				}

				//@formatter:off
				new AlertDialog.Builder(ActivityCourseEdit.this)
						.setTitle("Select class days")
						.setMultiChoiceItems(Util.DAY_LIST, daysCheck, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int indexSelected, final boolean isChecked) {
                            	if (isChecked) {
                            		selectedDays.add(indexSelected);
                            	} else if (selectedDays.contains(indexSelected)) {
                            		selectedDays.remove(Integer.valueOf(indexSelected));
                            	}
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialogInterface, final int which) {
                            	courseObj.clearDays();

                            	Day day;
                            	for (final Integer i : selectedDays) {
									day = Day.valueOf(Util.DAY_LIST[i].toUpperCase());
									courseObj.setDay(day, true);
                            	}
								setCourseString(daysText, courseObj.getSchedule().getDayStringCommasAbrv());
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
				//@formatter:on
			}
		});

		deleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				//@formatter:off
				new AlertDialog.Builder(ActivityCourseEdit.this)
						.setTitle("Delete Time")
                        .setMessage("Are you sure you wish to delete this course?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialogInterface, final int i) {
								final ScheduleDatabase sdb = ScheduleDatabase.getInstance(getApplicationContext());
								sdb.deleteCourse(courseObj);
                                finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
				//@formatter:on
			}
		});

		if (courseObj.getID().equals("")) {
			deleteButton.setVisibility(View.INVISIBLE);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_save_button, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
			case R.id.action_save_button:
				if (courseTitle.getText().length() == 0) {
					showHoldUpMessage("You must provide a course title!", false);
					return true;
				}
				if (courseSubject.getSelectedItem().toString().equals("Pick One")) {
					showHoldUpMessage("You should provide a course subject!", true);
					return true;

				} else if (courseNumber.getText().length() == 0) {
					showHoldUpMessage("You should provide a course number!", true);
					return true;
				} else if (courseSection.getText().length() == 0) {
					showHoldUpMessage("You should provide a course section!", true);
					return true;
				}
				saveCourseAndClose();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void hideKeyboard(final View view) {
		final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	private class EditTextFocusChangeStopEditing implements View.OnFocusChangeListener {
		@Override
		public void onFocusChange(final View view, final boolean hasFocus) {
			if (!hasFocus) {
				hideKeyboard(view);
			}
		}
	}

	/**
	 * Sets the TextView text to the hours and minutes defined by the ScheduleEntry
	 */
	private void updateTimeText() {
		startTimeText.setText(courseObj.getStartTime().to12HourString());
		endTimeText.setText(courseObj.getEndTime().to12HourString());
	}

	/**
	 * Creates a TimePickerDialog for hours and minutes
	 * @param tag the tag for the dialog
	 * @param time the time to update
	 */
	private void createTimeDialog(final String tag, final TimeObject time, final TimePickerDialog.OnTimeSetListener listener) {
		final TimePickerDialog tpd = TimePickerDialog.newInstance(listener, time.getHourOfDay(), time.getMinute(), false);
		tpd.setThemeDark(true);
		tpd.setAccentColor(getString(R.color.colorPrimaryDark));
		tpd.show(getFragmentManager(), tag);
	}

	/**
	 * Sets a TextView's string based on a supplied string
	 * Does not set if the string is empty, spaces not counted
	 * @param text the TextView to update
	 * @param courseString the supplied string
	 */
	private void setCourseString(final TextView text, final String courseString) {
		if (courseString.replaceAll(" ", "").length() > 0) {
			text.setText(courseString);
		} else {
			if (text == locationText) {
				text.setText("Set Course Location");
			} else if (text == daysText) {
				text.setText("Select Days of the Week");
			} else if (text == instructorText) {
				text.setText("Set Instructor Name");
			}
		}
	}

	private void saveCourseAndClose() {
		setResult(RESULT_OK);

		courseObj.setTitle(courseTitle.getText().toString());
		courseObj.setCourseSubject(courseSubject.getSelectedItem().toString());
		courseObj.setCourseNumber(courseNumber.getText().toString());
		courseObj.setSectionNumber(courseSection.getText().toString());
		courseObj.setInstructorName(instructorText.getText().toString());

		final ScheduleDatabase sdb = ScheduleDatabase.getInstance(getApplicationContext());
		Log.d(DAWGTAG, String.valueOf(sdb.updateCourse(courseObj)));

		finish();
	}

	private void showHoldUpMessage(final String warningMessage, final boolean continueAnyways) {
		//@formatter:off
		final AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityCourseEdit.this)
				.setTitle("Hold up!")
				.setMessage(warningMessage)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, final int i) {
                        // do nothing
                    }
                });
		//@formatter:on
		if (continueAnyways) {
			dialog.setNegativeButton("YOLO & Continue", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialogInterface, final int i) {
					saveCourseAndClose();
				}
			});
		}
		dialog.show();
	}
}
