/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.util;

import android.content.res.Resources;
import com.operontech.maroon.database.object.Day;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Pattern;

public class Util {
	final public static String[] DAY_LIST = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

	public static int dpToPx(final int dp) {
		return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static HashMap<Day, Boolean> convertDaysMapFromString(final String text) {
		final HashMap<Day, Boolean> data = new HashMap<>();
		final Pattern p = Pattern.compile("[\\{\\}\\=\\, ]++");
		final String[] split = p.split(text);
		for (int i = 1; i + 2 <= split.length; i += 2) {
			data.put(Day.valueOf(split[i]), Boolean.valueOf(split[i + 1]));
		}
		return data;
	}

	public static String createFileTimeStamp() {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss", Locale.US);
		return sdf.format(new Date());
	}
}
