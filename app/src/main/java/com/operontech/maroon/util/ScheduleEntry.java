/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.util;

import com.operontech.maroon.database.object.Day;
import com.operontech.maroon.database.object.TimeObject;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;

public class ScheduleEntry implements Serializable {
	HashMap<Day, Boolean> daysOfTheWeek;
	TimeObject startTime;
	TimeObject endTime;

	public ScheduleEntry() {
		daysOfTheWeek = new HashMap<>();
		clearDays();

		final int h = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		startTime = new TimeObject(h + 1, 0);
		endTime = new TimeObject(h + 2, 0);

	}

	public TimeObject getStartTime() {
		return startTime;
	}

	public TimeObject getEndTime() {
		return endTime;
	}

	public void setStartTime(final TimeObject timeObj) {
		startTime = timeObj;
	}

	public void setEndTime(final TimeObject timeObj) {
		endTime = timeObj;
	}

	/**
	 * Set the enabled status of a date in the DaysOfTheWeek map
	 * @param date the date
	 * @param enabled enabled/disabled
	 */
	public ScheduleEntry setDay(final Day date, final boolean enabled) {
		daysOfTheWeek.put(date, enabled);
		return this;
	}

	/**
	 * Sets all days to false
	 */
	public void clearDays() {
		for (final Day day : Day.values()) {
			daysOfTheWeek.put(day, false);
		}
	}

	/**
	 * Returns the days of the week defined
	 * @return day (Day object), true/false (Boolean)
	 */
	public HashMap<Day, Boolean> getDaysOfTheWeek() {
		return daysOfTheWeek;
	}

	/**
	 * Returns the DaysOfTheWeek map separated by commas abbreviated
	 * Only includes ENABLED days
	 * @return comma-separated String
	 */
	public String getDayStringCommasAbrv() {
		final StringBuilder builder = new StringBuilder();
		Day day;
		for (final String dayStr : Util.DAY_LIST) {
			day = Day.valueOf(dayStr.toUpperCase());
			if (daysOfTheWeek.get(day)) {
				if (builder.length() > 0) {
					builder.append(", ");
				}
				builder.append(day.getAbrv());
			}
		}
		return builder.toString();
	}

	/**
	 * Returns the DaysOfTheWeek map separated by commas
	 * Only includes ENABLED days
	 * @return comma-separated String
	 */
	public String getDayStringCommas() {
		final StringBuilder builder = new StringBuilder();
		Day day;
		for (final String dayStr : Util.DAY_LIST) {
			day = Day.valueOf(dayStr.toUpperCase());
			if (daysOfTheWeek.get(day)) {
				if (builder.length() > 0) {
					builder.append(", ");
				}
				builder.append(day.toString());
			}
		}
		return builder.toString();
	}
}
