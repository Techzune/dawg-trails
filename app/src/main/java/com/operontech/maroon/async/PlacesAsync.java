/*
 * Copyright (c) 2016 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.async;

import android.os.AsyncTask;
import com.operontech.maroon.R;
import com.operontech.maroon.database.PlacesDatabase;
import com.operontech.maroon.database.object.PlaceObject;
import com.operontech.maroon.frag.FragmentPlaces;

import java.util.ArrayList;

/**
 * Accesses the Places database and creates an adapter
 */
public class PlacesAsync extends AsyncTask<Void, Void, Void> {

	ArrayList<PlaceObject> places;
	FragmentPlaces fragPlaces;
	String target;

	public PlacesAsync(final FragmentPlaces fragPlaces, final String target) {
		super();
		this.fragPlaces = fragPlaces;
		this.target = target;
	}

	@Override
	protected Void doInBackground(final Void... params) {
		// Get the information from the database depending on the target
		places = new ArrayList<>();
		final PlacesDatabase db = new PlacesDatabase(fragPlaces.getContext());
		if (target.equalsIgnoreCase(fragPlaces.getString(R.string.type_residence_hall))) {
			places = db.getPlacesAsListFromTable(PlacesDatabase.TABLES.RESIDENCE_HALLS);
		} else if (target.equalsIgnoreCase(fragPlaces.getString(R.string.type_academic_hall))) {
			places = db.getPlacesAsListFromTable(PlacesDatabase.TABLES.ACADEMIC);
		} else if (target.equalsIgnoreCase(fragPlaces.getString(R.string.type_food_and_dining))) {
			places = db.getPlacesAsListFromTable(PlacesDatabase.TABLES.FOODANDDINING);
		} else if (target.equalsIgnoreCase(fragPlaces.getString(R.string.type_athletic))) {
			places = db.getPlacesAsListFromTable(PlacesDatabase.TABLES.ATHLETIC);
		} else if (target.equalsIgnoreCase(fragPlaces.getString(R.string.type_utility))) {
			places = db.getPlacesAsListFromTable(PlacesDatabase.TABLES.UTILITY);
		}
		db.close();
		return null;
	}

	@Override
	protected void onPostExecute(final Void aVoid) {
		super.onPostExecute(aVoid);
		// Set the adapter to the fragment
		if (fragPlaces != null) {
			fragPlaces.setupAdapter(places);
		}
	}
}
