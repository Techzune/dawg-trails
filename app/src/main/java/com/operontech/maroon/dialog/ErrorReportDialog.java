/*
 * Copyright (c) 2017 Jordan Stremming
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE
 */

package com.operontech.maroon.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import org.acra.dialog.BaseCrashReportDialog;

public class ErrorReportDialog extends BaseCrashReportDialog implements DialogInterface.OnDismissListener, DialogInterface.OnClickListener {

	@Override
	protected void init(@Nullable final Bundle savedInstanceState) {
		super.init(savedInstanceState);

		final AlertDialog dialog = new AlertDialog.Builder(this).setTitle("Dawg Trails: Oh No!")
		                                                        .setMessage("Something went wrong!\nPlease send us feedback so we can fix that.")
		                                                        .setPositiveButton("SEND FEEDBACK", this)
		                                                        .setNegativeButton("CANCEL", this)
		                                                        .create();

		dialog.setCanceledOnTouchOutside(false);
		dialog.setOnDismissListener(this);
		dialog.show();
	}

	@Override
	public void onDismiss(final DialogInterface dialog) {
		finish();
	}

	@Override
	public void onClick(final DialogInterface dialog, final int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			sendCrash(null, null);
		} else {
			cancelReports();
		}
		finish();
	}
}
